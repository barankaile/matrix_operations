/**
*	MatrixTest.java : Contains main for testing the accuracy of all public methods of Matrix.java
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 3
*	Course: CMPS 101
*
*/
public class MatrixTest
{
	public static void main(String[] args)
	{
		Matrix A = new Matrix(3);
		Matrix B = new Matrix(3);
		Matrix C;
		
		A.changeEntry(1,1,3.0);
		A.changeEntry(1,2,1.0);
		A.changeEntry(1,3,2.0);
		A.changeEntry(2,1,4.0);
		A.changeEntry(2,2,5.0);
		A.changeEntry(2,3,6.0);
		A.changeEntry(3,1,2.0);
		A.changeEntry(3,3,7.0);
		
		B.changeEntry(1,1,2.0);
		B.changeEntry(1,2,1.0);
		B.changeEntry(1,3,8.0);
		B.changeEntry(2,1,4.0);
		B.changeEntry(2,2,6.0);
		B.changeEntry(2,3,5.0);
		B.changeEntry(3,1,9.0);
		B.changeEntry(3,2,7.0);
		B.changeEntry(3,3,3.0);
		
		C = A.copy();
		
		System.out.println("A size: " + A.getSize());
		System.out.println("A NNZ: " + A.getNNZ());
		
		System.out.println("\n Matrix A: ");
		System.out.print(A);
		
		System.out.println("\n Matrix B: ");
		System.out.print(B);
		
		System.out.println("\n Matrix C: ");
		System.out.print(C);
		
		System.out.println("\n C.transpose(): ");
		System.out.print(C.transpose());
		
		System.out.println("\nA==C? " + A.equals(C));
		System.out.println("A==C.transpose()? " + A.equals(C.transpose()));
		
		C.makeZero();
		
		System.out.println("\nC NNZ: " + C.getNNZ());
		System.out.println(" Matrix C: ");
		System.out.print(C);
		
		System.out.println("\n A+B =");
		System.out.print(A.add(B));
		System.out.println("\n A-B =");
		System.out.print(A.sub(B));
		System.out.println("\n 2*A =");
		System.out.print(A.scalarMult(2));
		System.out.println("\n A*B =");
		System.out.print(A.mult(B));
		
	}
}